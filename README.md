# M2 LITL

Contact: chloe.braud@irit.fr

Slides, notebooks and data are available on: https://gitlab.irit.fr/melodi/andiamo/teaching_cbraud/master_litl 


* Basics of OOP: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1x1cDbQbu87RGFHRavwC2YRhGvNDj1MhK?usp=sharing)

* To load the notebook: TP1 Sentiment analysis with ScikitLearn [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1icJsbnjykYRpvNiJYJDDzip9RVWVhL-O?usp=sharing)
  * **You'll need to save a copy to your google drive or a notebook on your PC**
